package views;


import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import models.KeyboardButtonAction;
import models.deleteKeyAction;
import models.returnKeyAction;

/**
 * A class that extends JPanel and creates a specific
 * panel for the application's keyboard. Both the input and output field
 * have been instantiated in the class's constructor to allow them
 * to be manipulated when actions are fired.
 * 
 * @author Oladimeji Ogunyoye
 */

@SuppressWarnings("serial")
public class KeyboardButtonsPanel extends JPanel 
	{
		
	private JButton Q,W,E,R,T,Y,U,I,O,P,A,S,D,F,G,H,J,K,L,Z,X,C,V,B,N,M,zero,one,two,three,four,five,six,seven,eight,nine,dot,space,ret,del;            
	private JPanel KeyboardPanel;
	private JTextArea ins;
	private JEditorPane outputField;
	/**
	 * The class's constructor which initialises many of the
	 * application's main components. Such as the input and output
	 * field, as well as all the keyboard's keys.
	 * 
	 */
		public KeyboardButtonsPanel()
		{
		    KeyboardPanel = new JPanel(new GridLayout(4,4,7,7));
		    
		    ins = new JTextArea();
		    ins.setOpaque(false);
		    ins.setEditable(false);
		    
		    outputField = new JEditorPane();
		    outputField.setContentType("text/html");
		    outputField.setText("Welcome to the Stock Market Application! Your stock entries shall appear in the box to the right.");
		    outputField.setOpaque(false);
		    outputField.setEditable(false);
		    
		    outputField.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, Boolean.TRUE);
		    Font f = new Font("Helvetica", Font.PLAIN, 11);
		    outputField.setFont(f);
		    
		 
			JButton[] keys = {zero,one,two,three,four,five,six,seven,eight,nine,Q,W,E,R,T,Y,
					            U,I,O,P,A,S,D,F,G,H,J,K,L,Z,X,C,V,B,N,M,dot,space};
			
			String[] labels = {"0","1","2","3","4","5","6","7","8","9","Q","W","E","R","T","Y","U","I","O","P",
					            "A","S","D","F","G","H","J","K","L","Z","X","C","V","B","N","M","."," "}; // the blank label string represents the space bar
			
			for(int i = 0; i < keys.length; i++)
			{
				keys[i] = new JButton(labels[i]);
				KeyboardPanel.add(keys[i]);
				keys[i].addActionListener(new KeyboardButtonAction(ins));
			}
			
			del = new JButton("Del");
			KeyboardPanel.add(del);
			del.addActionListener(new deleteKeyAction(ins));
			
			ret = new JButton("RETURN");
			KeyboardPanel.add(ret);
			ret.addActionListener(new returnKeyAction(ins,outputField));		
		}
		/**
		 * A method that returns the keyboard panel to the application's main window
		 * 
		 * @return JPanel of the app's keyboard
		 */
		public JPanel getKeyboardPanel()
		{
			return KeyboardPanel;
		}
		
		/**
		 * A method that returns the application's input field
		 * 
		 * @return ins - JTextArea input field
		 */
		public JTextArea getInputfield()
		{
			return ins;
		}
		/**
		 * A method that returns the application's output field
		 * 
		 * @return outputField - JEditorPane output field
		 */
		public JEditorPane getOutputfield()
		{
			return outputField;
		}


	}