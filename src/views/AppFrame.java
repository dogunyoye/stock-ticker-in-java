package views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * A class that sets up the fundamental aspects of the GUI 
 * that the user can interact with.
 * 
 * @author Oladimeji Ogunyoye
 *
 */

@SuppressWarnings("serial")
public class AppFrame extends JFrame 
{
    /**
     * AppFrame's constructor which sets up the application's window
     * and widgets contained in them.
     * 
     */
	public AppFrame()
	{
		super("Stock Market Application");
		setPreferredSize(new Dimension(937,323)); // specific dimensions have been set to the frame to allow the app's GUI to be displayed neatly.
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		appWidgets();
		
		pack();
		setVisible(true);
	}
	
	/**
	 * A method that sets up most of the app's widgets and
	 * attributes specific layout managers to each of them.
	 * 
	 */
	public void appWidgets()
	{
		JLabel Output = new JLabel("Output:  ");
		JLabel info = new JLabel("    Please separate multiple stocks using the space key to the left of the delete button");
		JPanel topGrid = new JPanel();
		topGrid.setLayout(new BoxLayout(topGrid, BoxLayout.LINE_AXIS));
		add(topGrid, BorderLayout.NORTH);
		
		JPanel midPane = new JPanel();
		midPane.setLayout(new BoxLayout(midPane, BoxLayout.LINE_AXIS));
		add(midPane, BorderLayout.CENTER);
		
        KeyboardButtonsPanel appKeyboard = new KeyboardButtonsPanel();
        
        JTextArea txtA = appKeyboard.getInputfield();
        txtA.setLineWrap(true);
        
		topGrid.add(Output);
		topGrid.add(appKeyboard.getOutputfield());
		midPane.add(info);
		midPane.add(txtA);
		
		JScrollPane scroll_pane = new JScrollPane(appKeyboard.getOutputfield());
		JScrollPane scroll_pane1 = new JScrollPane(txtA);
		scroll_pane.setPreferredSize(new Dimension(250, 145));
		scroll_pane1.setPreferredSize(new Dimension(50,145));
		topGrid.add(scroll_pane);
		topGrid.add(scroll_pane1);
		
		JPanel bottomPane = new JPanel(new FlowLayout());
		add(bottomPane, BorderLayout.SOUTH);
		bottomPane.add(appKeyboard.getKeyboardPanel());
	}
}
