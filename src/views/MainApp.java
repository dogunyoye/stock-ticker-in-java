package views;

/**
 * A Stock Market Application that allows a user to 
 * input a stock symbol (or a combination of stock symbols each separated by a space),
 * and returns a set of information as a result. Such as the company name,
 * price change, dividend, market capitalisation and the stock 
 * exchange of the requested stock(s).
 * 
 * @author Oladimeji Ogunyoye
 *
 */
public class MainApp 
{
     public static void main(String[] args)
     {
    	 new AppFrame();
     }
}
