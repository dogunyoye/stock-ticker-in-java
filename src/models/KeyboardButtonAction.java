package models;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

/**
 * A class that returns the key pressed for all letters, numbers and 
 * symbols (except the delete and return key).
 * 
 * @author Oladimeji Ogunyoye
 */

public class KeyboardButtonAction implements ActionListener 
{
	private JTextArea tArea;
	/**
	 * Constructor which accepts the JTextArea to update with text as a 
	 * parameter
	 * 
	 * @param tArea - the text area to append characters to.
	 */
	public KeyboardButtonAction(JTextArea tArea)
	{
		this.tArea = tArea;
	}
/**
 * Inherited abstract method from the ActionListener interface
 * that appends letters, numbers and symbols to the input area in the application
 */
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		tArea.append(e.getActionCommand());
	}

}
