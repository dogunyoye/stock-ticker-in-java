package models;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

/**
 * This class provides the action for a delete key - taking the text currently 
 * in the input field as a parameter in the constructor.
 * 
 * @author Oladimeji Ogunyoye
 */

public class deleteKeyAction implements ActionListener 
{
	private JTextArea inField;
	/**
	 * Constructor which accepts a JTextArea as a parameter. The app's
	 * input field will be passed into this parameter seeing as that is 
	 * the area we're deleting text from
	 * 
	 * @param inField - the text area to delete characters from.
	 */
	public deleteKeyAction(JTextArea inField)
	{
		this.inField = inField;
	}
	
  /**
   * Inherited abstract method from the ActionListener interface
   * that has been constructed to replace the character at the
   * highest index in the input field with an empty string. Hereby
   * deleting the character one at a time.
   */
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		if(inField.getText().length() > 0) // this loop prevents the user from deleting nothing from an empty input field.
		{
		inField.replaceRange("", inField.getText().length()-1, inField.getText().length());
		}
	}

}
