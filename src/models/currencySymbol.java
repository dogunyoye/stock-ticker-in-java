package models;

/**
 * A class with the purpose of returning the correct currency symbol 
 * depending on what string (Stock exchange) is passed into the string parameter.
 * 
 * @author Oladimeji Ogunyoye
 */

public class currencySymbol 
{
	private String symbol;
	
	/**
	 * This constructor instantiates the string symbol 
	 * as an empty string
	 */
	public currencySymbol()
	{
		symbol = "";
	}
	
	/**
	 * A method that returns the currency symbol belonging
	 * to a specific stock - determined by its stock exchange.
	 * 
	 * @param stockExchange - the stock exchange of the specified stock
	 * 
	 * @return the currency symbol of the specified stock
	 */
	public String getCurrencySymbol(String stockExchange)
	{
		
		if(stockExchange.contains("NasdaqNM") || stockExchange.contains("NYSE"))
		{
			symbol = "$";
		}
		else if(stockExchange.contains("Brussels") || stockExchange.contains("Paris"))
		{
			symbol = "EUR";
		}
		else if(stockExchange.contains("HKSE"))
		{
			symbol = "HK$";
		}
		else if(stockExchange.contains("London"))
		{
			symbol = "p(ence)";
		}
		else if(stockExchange.contains("SES"))
		{
			symbol = "S$";
		}
		
		return symbol;
	}

}
