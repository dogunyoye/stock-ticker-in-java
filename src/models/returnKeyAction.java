package models;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.JEditorPane;
import javax.swing.JTextArea;

/**
 * A class that implements an action for the return key. The stock symbol(s) in the input field 
 * is queried and depending on if the stock symbol(s) is valid or not, the return key will 
 * update the output field with the relevant stock(s) information. Else, the output
 * field is updated with "Symbol not found".
 * 
 * @author Oladimeji Ogunyoye
 *
 */

public class returnKeyAction implements ActionListener 
{
	private JTextArea inArea;
	private JEditorPane outArea;
	private String[] temp;
	private String data;
	/**
	 * Constructor which requires the application's
	 * JTextArea (input field) and JEditorPane (output field).
	 * Seeing as both are updated with information when the 
	 * return key is pressed.
	 * 
	 * @param inArea - input field
	 * @param outArea - output field
	 */
	public returnKeyAction(JTextArea inArea, JEditorPane outArea)
	{
		this.inArea = inArea;
		this.outArea = outArea;
	}
	
	/**
	 * A method (built specifically for multiple queries) that splits 
	 * the data retrieved from the query into an array
	 * 
	 * @return An array where each index has data for each stock queried.
	 */
	public String[] getMultData()
	{
		data = inArea.getText().replace(' ', '+'); // replaces any spaces with '+' symbols to transform the entry into a string that can be queried.
		String stockData = URLReader.readURL("http://finance.yahoo.com/d/quotes.csv?s=" + data + "&f=dj1l1pxn");
		temp = stockData.split("\n");
		
		return temp;
		
	}
	
	/**
	 * A method that lays out the relevant parts of data gathered for 
	 * the Stock Market Application. The Market Capitalisation of the stock
	 * queried has been used as an "error flag", seeing as the official error flag,
	 * "e1" fails (in some cases) to pick up invalid stock queries.
	 * 
	 * @param x - The data gathered from the query (separated by commas)
	 * 
	 * @return The company name, price change, dividend, market capitalisation
	 *         and stock exchange as a string (or if the stock is not valid, "Symbol not found"
	 *         is returned).
	 */
	public String arrangeData(String x)
	{
		double firstNumber,secondNumber,priceChange = 0,priceChangePercentage = 0;
		data = "";
		String rowOfStars = "*********************";
		currencySymbol symbol = new currencySymbol(); // instantiates "symbol" as an instance of the currencySymbol class to later obtain the stock's currency symbol.
		temp = x.split(",", 7); // the split limit of 7 ensures that company names with commas aren't split into the array
		
		if(temp[1].contains("N/A")) // catches any invalid stock queries if the market cap is "N/A". This was used instead of the error flag to prevent missing invalid stock symbols.
		{
		    data = "ERROR: Symbol is not valid!" + "<p>" + "</p>";
			inArea.setText("");
			return data;
		}
		
		else
		{	
			
		firstNumber = Double.parseDouble(temp[2]);
	    secondNumber = Double.parseDouble(temp[3]);
		priceChange = firstNumber - secondNumber;
		priceChangePercentage = ((firstNumber - secondNumber)/secondNumber)*100;
		
		DecimalFormat twoDP = new DecimalFormat("0.00");  // rounds the price change to two decimal places.
		String pChangePercentage = twoDP.format(priceChangePercentage);
		String pChange = twoDP.format(priceChange);
		
		if(priceChangePercentage > 0) // if the price change percentage is greater than 0, then the price's colour is set to green
		{
			pChangePercentage = "<font color=\"green\">(+" + pChangePercentage + "%)</font>";
			pChange = "<font color=\"green\">+" + pChange + "</font>";
		}
		else if(priceChangePercentage < 0) // if the price change percentage is less than 0, then the price's colour is set to red
		{
			pChangePercentage = "<font color=\"red\">(" + pChangePercentage + "%)</font>";
			pChange = "<font color=\"red\">" + pChange + "</font>";
		}
		else // if neither of the above conditions are satisfied, the colour stays black
		{
			pChangePercentage = "(" + pChangePercentage + "%)";
			pChange = "" + pChange;
		}
	
     data = (temp[5].replaceAll("\"", "").toUpperCase() + "<br>" + rowOfStars + "</br>" + "<br>" + "Price: "  + temp[3] + symbol.getCurrencySymbol(temp[4]) + "</br>" +
     "<br>" + "Change: " + pChange  + pChangePercentage + "</br>" + "<br>" + "Divident: " + temp[0] + "</br>" + "<br>" + "Market Cap: " + temp[1] + "</br>"
     + "<br>" + "Stock Exchange: " + temp[4].replaceAll("\"", "") + "</br>" + "<p>" + "</p>");
		}
		
		return data;
	}
	
	/**
	 * Inherited abstract method from the ActionListener interface
	 * that implements an action for the "return" key. Text 
	 * is collected from the input field and passed as a parameter
	 * for the getInfo method, which eventually updates the 
	 * output area with the relevant stock information (if the
	 * stock symbol is valid).
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		String x = inArea.getText();
		JTextArea dummy = new JTextArea(); // this textArea acts a temporary space for the data
		
		if(inArea.getText().contains(" ")) //  queries multiple stock entries
		{
			String[] mx = getMultData(); // when the return key is pressed, if this condition is satisfied, getMult is called which queries the stocks
			for(int i = 0; i < mx.length; i++)
			{
				dummy.append(arrangeData(mx[i]));
			}
			
			outArea.setText(dummy.getText());
			inArea.setText(""); // clears the input field after each query
		}
		else
		{
		String stockData = URLReader.readURL("http://finance.yahoo.com/d/quotes.csv?s=" + x + "&f=dj1l1pxn"); // queries single stock entries
		outArea.setText(arrangeData(stockData));
		inArea.setText("");
		}
	  }
	}
	